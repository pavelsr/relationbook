$( document ).ready(function() {
    lazy_oader();
    
    $('#sex-switcher li').click(function(){
        $('#sex-switcher li').removeClass('active');    
        $(this).addClass('active');
        var $radios = $(this).find('input:radio');
        if($radios.is(':checked') === false) {
            //$radios.change();
            $radios.prop('checked', true);
            do_filter();
        }
    });
    
    $('#search-fild').bind('keyup change', function(ev) { 
        var searchQuery = $(this).val();
        jQuery.expr[":"].contains = function( elem, i, match, array ) {
            return (elem.textContent || elem.innerText || jQuery.text( elem ) || "").toLowerCase().indexOf(match[3].toLowerCase()) >= 0;
        }

        if (searchQuery.length > 1) {
            $('#main .unit').hide();
            $('#main .person-name:contains("'+searchQuery+'")').closest('.unit').show();
        } else {
            $('#main .unit').show();
        }
        var current_count = $(".unit:visible").length;
        console.log(current_count);
        $("#ret_results").html(current_count);
    });
    
    if ($('body > .container').height()<$(document).height()) {
        $('body > .container').height($(document).height()-$('#footer').height()-50)
    }
});

function lazy_oader() {
    console.log('lazy-start')
    $("#main").lazyScrollLoading({
	isDefaultLazyImageMode : true
    });
}