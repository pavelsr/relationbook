# # coding: utf-8

# from django import forms
# from django.contrib.auth import authenticate


# class RegistrationForm(forms.ModelForm):
#     password1 = forms.CharField(label = u'Пароль', min_length = 7, max_length = 64, widget = forms.PasswordInput)
#     password2 = forms.CharField(label = u'Ещё раз', help_text = u'Пароль вводится дважды для проверки', widget = forms.PasswordInput)

#     class Meta:
#         model = User
#         fields = ['username', 'password1', 'password2']

#     def clean_username(self):
#         username = self.cleaned_data['username'].lower()
#         if User.objects.filter(username = username).count():
#             raise forms.ValidationError(u'Пользователь с таким именем уже зарегистрирован.')
#         if not re.match(r'^\w+$', username):
#             raise forms.ValidationError(u'В имени пользователя можно использовать только латинские буквы, цифры и знак подчёркивания.')
#         return username

#     def clean_password2(self):
#         try:
#             password1 = self.cleaned_data['password1']
#         except KeyError:
#             return ''
#         password2 = self.cleaned_data['password2']
#         if password1 != password2:
#             raise forms.ValidationError('Пароли не совпадают.')
#         return password2

#     def save(self):
#         super(RegistrationForm, self).save(commit = False)
#         data = self.cleaned_data
#         self.instance.set_password(data['password1'])
#         self.instance.save()


# class LoginForm(forms.Form):
#     username = forms.CharField(label = u'Имя пользователя', max_length = 255)
#     password = forms.CharField(label = u'Пароль', min_length = 7, max_length = 64, widget = forms.PasswordInput)

#     def clean_password(self):
#         data = self.cleaned_data
#         user = authenticate(username = data.get('username'), password = data.get('password'))
#         if user is None:
#             raise forms.ValidationError(u'Введены неверные логин/пароль')
#         elif not user.is_active:
#             raise forms.ValidationError(u'Пользователь заблокирован')
#         else:
#             self.user = user
#             return data.get('username')




# # EOF
