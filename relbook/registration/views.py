# coding: utf-8

import requests

from django.conf import settings
from django.http import HttpResponse
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.contrib.auth import login
from django.shortcuts import render_to_response


# from .forms import LoginForm
from .models import FriendList, relation_choices, sex_choices

# def user_login(request):
#     if request.method == 'POST':
#         form = LoginForm(request.POST)
#         if form.is_valid():
#             login(request, form.user)
#             return HttpResponseRedirect('/')
#     else:
#         form = LoginForm()
#     return render_to_response('/index.html', {'form': form}, RequestContext(request))


# def register(request):
#     pass

def make_rel_string(unit):
    rel = int(unit.get('relation', '0'))
    sex = int(unit.get('sex', '0'))
    rel_text = relation_choices.get(rel, '')
    if rel and sex:
        if sex == 1:
            rel_text = rel_text.split('/')[-1]
        elif sex == 2:
            rel_text = rel_text.split('/')[0]
        if unit.get('relation_partner', False):
            if rel in [2, 3, 5]:
                # с
                if rel == 2:
                    rel_text = u'встречается'
                x = u'с'
            elif rel in [4]:
                # за, на
                if sex == 1:
                    x = u'за'
                else:
                    x = u'на'
            elif rel in [7]:
                # в
                x = u'в'
            rel_text += u' %s <a href="http://vk.com/id%s">%s %s</a>' %(
                x,
                unit.get('relation_partner', {}).get('id', ''),
                unit.get('relation_partner', {}).get('first_name', ''),
                unit.get('relation_partner', {}).get('last_name', ''),
                )
    return rel_text

def update_friendlist(user):
    blank_url = u'https://api.vk.com/method/%s'
    method_name = u'friends.get'
    social_data = user.social_auth.all()[0]
    fields_list = ['relation', 'sex', 'photo_50', 'photo_100', 'screen_name']
    params = dict(
        fields = ','.join(fields_list),
        access_token = social_data.extra_data.get('access_token', ''),
        )
    print 'send request...'
    response = requests.post(blank_url %method_name, params).text
    print 'done'
    response_dict = simplejson.loads(response)
    changed_list = list()
    for i in response_dict.get('response', []):
        i.pop('online')
        try:
            unit = user.friend_set.get(vk_id = i.get('uid'))
            if unit.data == i:
                print 'unit %s\t not modifed' %i.get('uid')
            else:
                print i
                print unit.data
                print 'unit %s\t modifed' %i.get('uid')
                if unit.relation != int(i.get('relation', '0')):
                    changed_list.append(dict(
                        unit = unit,
                        old = make_rel_string(unit.data),
                        new = make_rel_string(i),
                        ))
                    print '\n\tCHANGE!\n'
                    unit.is_changed = True
                unit.relation = i.get('relation', '0')
                unit.sex = i.get('sex', '0')
                unit.data = i
                if not settings.UPDATE_FRIENDLIST_DEBUG:
                    unit.save()
        except (FriendList.MultipleObjectsReturned, FriendList.DoesNotExist):
            unit = user.friend_set.create(
                vk_id = i.get('uid'),
                relation = i.get('relation', '0'),
                sex = i.get('sex', '0'),
                data = i,
                )
            print 'unit %s\t created' %i.get('uid')
    return changed_list


@login_required
def get_friendlist(request):
    print 'start'
    update_friendlist(request.user)
    rel_filter = request.GET.getlist('rel', None)
    sex_filter = request.GET.get('sex', '-1')
    filter_kwargs = dict()
    if rel_filter:
        filter_kwargs.update(relation__in = [int(i) for i in rel_filter])
    if 0 <= int(sex_filter) <= 2:
        queryset = request.user.friend_set.filter(sex = int(sex_filter))
    else:
        queryset = request.user.friend_set.all()
    print 'filters:', filter_kwargs
    friend_list = queryset.filter(**filter_kwargs)
    result = list()
    for i in friend_list.values('data', 'is_changed'):
        i = dict(simplejson.loads(i.pop('data')), **i)
        rel_text = make_rel_string(i)
        i.update(
            rel_stat = i.get('relation'),
            relation = rel_text,
            )
        result.append(i)
    rel_dict = dict()
    for i in set(request.user.friend_set.all().values_list('relation', flat = True)):
        rel_dict.update({i: u'%s (%d)' %(relation_choices.get(i), queryset.filter(relation = i).count())})

    friend_list.update(is_changed = False)
    return render_to_response('results.html', {'result': result, 'rel_dict': rel_dict}, RequestContext(request))



def tracklist(request):
    print request.GET
    uid_list = [int(i) for i in request.GET.getlist('uid', [])]
    del_uid = int(request.GET.get('del', '0'))
    if uid_list:
        email = request.GET.get('email', '')
        phone = request.GET.get('phone', '')
        request.user.email = email
        request.user.phone = phone
        request.user.save()
        request.user.friend_set.exclude(vk_id__in = uid_list).update(track = False)
        request.user.friend_set.filter(vk_id__in = uid_list).update(track = True)
    elif del_uid > 0:
        request.user.friend_set.filter(vk_id__in = [del_uid]).update(track = False)
    result = list()
    for i in request.user.friend_set.filter(track = True).values('data', 'is_changed'):
        i = dict(simplejson.loads(i.pop('data')), **i)
        rel_text = make_rel_string(i)
        i.update(
            rel_stat = i.get('relation'),
            relation = rel_text,
            )
        result.append(i)
    return render_to_response('tracklist.html', {'result': result}, RequestContext(request))


# EOF
