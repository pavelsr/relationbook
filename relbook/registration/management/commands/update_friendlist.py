# coding: utf-8

from django.conf import settings
from django.core.mail import send_mail
from django.core.management.base import NoArgsCommand
from django.template.loader import render_to_string

from ....core.models import User
from ...models import FriendList
from ...views import update_friendlist, make_rel_string


class Command(NoArgsCommand):
    help = u'''Обновление семейного положения друзей'''

    def handle_noargs(self, **options):
        user_list = User.objects.filter(is_active = True, is_superuser = False, is_staff = False)
        for user in user_list:
            change_list = update_friendlist(user)
            track_list = user.friend_set.filter(track = True).values_list('vk_id', flat = True)
            print track_list
            if change_list:# and user.email:
                print 'sending_email...'
                object_list = list()
                for i in change_list:
                    if i.get('unit').vk_id in track_list:
                        object_list.append(dict(
                            name = i.get('unit'),
                            old = i.get('old'),
                            new = i.get('new'),
                            have_chance = i.get('unit').relation in [0, 5],
                            ))
                context = dict(
                    object_list = object_list,
                    )
                subject = settings.EMAIL_SUBJECT_PREFIX + u' Обновление'
                message = render_to_string('email_notification.txt', context)
                print message
                print subject
                print settings.DEFAULT_FROM_EMAIL
                print user.email
                send_mail(
                    subject,
                    message,
                    settings.DEFAULT_FROM_EMAIL,
                    [user.email],
                    # content_subtype = "html",
                    )

# EOF
